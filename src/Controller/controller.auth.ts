import { json, Request, Response } from "express";
import { QueryResult } from "pg";
import { pool } from "../database";
import jwt from "jsonwebtoken";
import { comparePassword, encrypt } from "../Function/bcryptPassword";

export const registraEstudiante = async (req: Request, res: Response) => {
    const { ci, nombre, ap_paterno, ap_materno, correo, fono, institucion, fecha_nacimiento } = req.body;
    const password = await encrypt(req.body.password);
    console.log(password);
    try {
        const queryRegistrar: QueryResult = await pool.query("Insert into Persona(ci,nombre,ap_paterno, ap_materno,correo,fono, password) values($1,$2,$3,$4,$5,$6,$7)", [ci, nombre, ap_paterno, ap_materno, correo, fono, password]);
        pool.query("SELECT * FROM Persona WHERE ci=$1 and nombre= $2 and ap_paterno = $3 and correo= $4 and fono = $5 and  password = $6", [ci, nombre, ap_paterno, correo, fono, password])
            .then(async (data) => {
                if (data) {
                    var datalist = data.rows.map((datos) => {
                        return datos;
                    });
                    //console.log(datalist[0].id_persona);
                    await pool.query("Insert into Estudiante(id_persona, institucion, fecha_nacimiento) values ($1, $2,$3)", [datalist[0].id_persona, institucion, fecha_nacimiento])
                        .then(
                            async (data) => {
                                await pool.query("SELECT * FROM Estudiante Where id_persona = $1", [datalist[0].id_persona])
                                    .then(
                                        async (data) => {

                                            var listData = data.rows.map((dat) => {
                                                return dat;
                                            });
                                            //console.log(listData[0]);
                                            await pool.query("SELECT * FROM Estudiante p, Persona r WHERE p.id_persona = r.id_persona and p.id_persona =$1 and r.id_persona = $2",
                                                [datalist[0].id_persona, listData[0].id_persona])
                                                .then(
                                                    (data) => {
                                                        //console.log(data);
                                                        res.status(200).json(data.rows);
                                                    }
                                                ).catch(
                                                    (err) => {
                                                        if (err)
                                                            res.status(400).json({ message: "Internal Server Error pers", err });
                                                    }
                                                );
                                        }
                                    ).catch(
                                        (err) => {
                                            res.status(400).json({ message: "Internal Server Error", err });
                                        });
                            }
                        )
                        .catch(
                            (err) => {
                                res.status(400).json({ message: "Internal Server Error", err });
                            }
                        );
                } else
                    res.status(400).json({
                        message: "Not Data",
                    });
            })
            .catch((err) => {
                res.status(400).json({ message: "Internal Server Error", err });
            });

    } catch (err) {
        res.status(400).json({ message: "Internal Server Error", err });
    }
};
export const login = async (req: Request, res: Response) => {
    const {correo , password } = req.body;

    await pool.query("SELECT * FROM Persona Where correo = $1 ",[correo])
    .then(
        async(data)=>{
            if(data){
                var listEst = data.rows.map(
                    (datae)=>{return datae;}
                );
                
                if(await comparePassword(password, listEst[0].password)){

                    const token: string = jwt.sign({ id: listEst[0].id_persona }, "TOkEN_TESTING");

                    res.header("token-auth", token).status(200).json(listEst[0]);
                }
                
            }else   
                res.status(404).json("not data");
        }
    )
    .catch(
        (err)=>{
            res.status(400).json({message : "Internal Server Error",err});
        }
    );
}
export const profile = async (req: Request, res: Response) => {
    try {
        const response: QueryResult = await pool.query('SELECT * FROM Persona WHERE id_persona = $1', [req.userId]);
        if (response.rowCount != 0) {
            return res.status(200).json(response.rows);
        } else
            return res.status(404).json('Data not found')
    } catch (error) {
        return res.status(400).json('internal server error')
    }
}