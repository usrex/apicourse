import bcryptjs from "bcryptjs";
export const encrypt = async (passwod :string): Promise<string>=>{
    const salt = await bcryptjs.genSalt(10); // el algoritmo se ejecuta 10 veces 

    return bcryptjs.hash(passwod,salt);

};

export const comparePassword = async (passwordCliente:string, passwodDB:string):Promise<boolean>=>{
    return await bcryptjs.compare(passwordCliente, passwodDB);

}