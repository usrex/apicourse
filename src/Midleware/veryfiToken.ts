import { Request,Response,NextFunction } from "express";
import jwt from 'jsonwebtoken';

interface IPayload{
    id:string,
    iat:number,
    exp:number
}

export const TokenValidator = (req:Request, res:Response, next:NextFunction)=> {

    const token = req.header('auth-token');
    if(!token){
        return res.status(401).json('acess denied');
    }
    const payload = jwt.verify(token, "TOkEN_TESTING") as IPayload;
    //console.log(payload);
    req.userId = payload.id;
    next();
}