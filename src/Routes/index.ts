import { Router } from "express";
import routauth from "./route.auth"

const route = Router();

route.use(routauth);

export default route;