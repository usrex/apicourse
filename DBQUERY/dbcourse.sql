
CREATE TABLE Persona(
    id_persona SERIAL PRIMARY KEY,
    ci INTEGER NOT NULL UNIQUE,
    nombre varchar(25),
    ap_paterno varchar(25),
    ap_materno varchar(25),
    correo varchar(25),
    Fono varchar(25),
    password varchar(150)
);

CREATE TABLE Tutor(
    id_persona SERIAL PRIMARY KEY,
    cargo varchar(25),
    CONSTRAINT fk_id_tutor FOREIGN KEY (id_persona) REFERENCES Persona (id_persona) 
);

CREATE TABLE Estudiante(
    id_persona SERIAL PRIMARY KEY,
    institucion varchar(50),
    fecha_nacimiento DATE,
    CONSTRAINT fk_id_estudiante FOREIGN KEY (id_persona) REFERENCES Persona (id_persona) 
);
CREATE TABLE Curso(
    id_curso SERIAL PRIMARY KEY,
    descripcion varchar(500),
    nombre varchar (250)
);

CREATE TABLE Tuto_toma_asistencia(
   id_persona_tutor INTEGER ,
   id_persona_est INTEGER ,
    id_curso SERIAL,
   fecha_asiste DATE,
   CONSTRAINT fk_tutor FOREIGN KEY (id_persona_tutor) REFERENCES Tutor (id_persona) ,
   CONSTRAINT fk_id_curso FOREIGN KEY (id_curso) REFERENCES Curso (id_curso) ,
   CONSTRAINT fk_estudiante FOREIGN KEY (id_persona_est) REFERENCES Estudiante (id_persona) 
);


CREATE TABLE Tutor_imparte_curso(
    id_persona_tutor INTEGER,
    id_curso INTEGER,
    semestre varchar(15),
    anio varchar(4),
    direccion varchar(250),
    CONSTRAINT fk_tutor_imparte FOREIGN KEY (id_persona_tutor) REFERENCES Tutor (id_persona) ,
    CONSTRAINT fk_curso FOREIGN KEY (id_curso) REFERENCES Curso (id_curso) 
);

