"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.pool = void 0;
const pg_1 = require("pg");
exports.pool = new pg_1.Pool({
    user: 'postgres',
    host: 'localhost',
    password: 'Hy680dra',
    database: 'CourseDev'
});
/*
export const pool = new Pool({
  user    : 'pmhygoquqrhdit'    ,
  host    : 'ec2-52-44-235-121.compute-1.amazonaws.com'   ,
  password: '04e61974ef901d1a7991c6072d44fbf94ad0df66baeb80364d57277e8f59431c'    ,
  database: 'ddobqfh1q884rb',
  port    : 5432,
  ssl: { rejectUnauthorized: false },
  connectionString:'postgres://pmhygoquqrhdit:04e61974ef901d1a7991c6072d44fbf94ad0df66baeb80364d57277e8f59431c@ec2-52-44-235-121.compute-1.amazonaws.com:5432/ddobqfh1q884rb',
});
*/
exports.pool.connect((err, client, release) => {
    if (err) {
        return console.error('Error acquiring client', err.stack);
    }
    client.query('SELECT NOW()', (err, result) => {
        release();
        if (err) {
            return console.error('Error executing query', err.stack);
        }
        console.log(result.rows);
    });
});
