"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const route_auth_1 = __importDefault(require("./route.auth"));
const route = express_1.Router();
route.use(route_auth_1.default);
exports.default = route;
